console.log("Hello, Feb!")

// For selecting HTML elements, we will be using the document.querySelector()
// Syntax: document.querySelector("htmlElement")
// document - refers to the whole page
// querySelector - used to select a specific object(HTML element from the document/webpage)
	// The querySelector takes a string input that is formatted like a CSS selector when applying the styles
const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// Event Listener
	// Whenever a user interacts with a webpage, this action is considered as an "event".
	// To perform an action when an event triggers, we need to listen to it.
	// The method addEventListener takes two arguments:
		// - A string identifying an event
		// - A function that the listener will execute once the "event" is triggered
// "event" contains the information on the triggered event

txtFirstName.addEventListener("keyup", (event) => {

	// .value - sets or returns the value of the element
	// .innerHTML - sets or returns the value of an attribute of an element
	spanFullName.innerHTML = txtFirstName.value;
});

txtFirstName.addEventListener("keyup", (event) => {

	// The event.target contains the element where the event happened
	console.log(event.target);

	// The event.target.value gets the value of the input object
	console.log(event.target.value);
});

txtLastName.addEventListener("keyup", (event) => {

	spanFullName.innerHTML = txtLastName.value;
});


// ACTIVITY
function updateFullName() {
	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
};

(txtFirstName, txtLastName).addEventListener("keyup", updateFullName);
